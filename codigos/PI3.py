
from pyfirmata import Arduino, util
from pyfirmata import SERVO
from tkinter import *

board = Arduino('COM4')

board.digital[6].mode = SERVO

def moverServo(a):
	board.digital[6].write(a)

root = Tk()
 
scale = Scale(root,
    command = moverServo,
    to = 175,
    orient = VERTICAL,
    length = 400,
    label = 'angulo')
scale.pack(anchor = CENTER)

root.mainloop()